var sql = require('mssql');

var config = {
    server: '192.168.56.1',
    database: 'Chinook',
    user: 'sa',
    password: 'p',
    port: 1433
};

// sql.connect(config, err => {

//     var request = new sql.Request();
//     request.stream = true;
//     request.query('select top 3 * from artist');

//     request.on('row', row => {
//         console.log(row.ArtistId +' : '+ row.Name);
//     });
// });

var pool1 = new sql.ConnectionPool(config, err => {
    var request = pool1.request();
    
    request.input('Lower', sql.Numeric, 1);
    request.input('Upper', sql.Numeric, 2);
    
    request.execute('Track_SelectByUnitPirce', 
                    (err, result, returnValue) => {
                        // 전체 로우 수
                        console.log(result.rowsAffected);
                        result.recordset.forEach(function(row) {
                            console.log(`${row.Name} is ${row.UnitPrice}`);
                        }, this);
    });
});